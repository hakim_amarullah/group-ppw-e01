import datetime
from django.db import models

KATEGORI_CHOICES = (
    ('Umum', 'Umum'),
    ('Kesehatan', 'Kesehatan'),
    ('Teknologi', 'Teknologi'),
    ('Politik', 'Politik'),
    ('Ekonomi', 'Ekonomi'),
    ('Sosial', 'Sosial'),
    ('Pendidikan', 'Pendidikan'),
    ('Makanan', 'Makanan'),
    ('Alam', 'Alam'),
)

# Create your models here.
class Articles(models.Model):
    penulis = models.CharField(max_length=20, null=True)
    kategori = models.CharField(max_length=20, choices=KATEGORI_CHOICES, default='Umum')
    tanggal = models.DateField()
    judul = models.CharField(max_length=100)
    isi_artikel = models.TextField(max_length=1000)
    gambar_artikel = models.ImageField(default="virusbiru2.svg")

    def __str__(self):
        return self.judul

