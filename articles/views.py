from django.shortcuts import render, redirect
from .models import Articles
from .forms import ArticlesForm
from django.http import HttpResponse


# Create your views here.
def articles_page(request):
    semua_artikel = Articles.objects.all()
    jumlah_semua_artikel = semua_artikel.count()
    context = {'semua_artikel': semua_artikel, 'jumlah_semua_artikel': jumlah_semua_artikel}
    return render(request, 'articles.html', context)

def insert_articles(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = ArticlesForm(request.POST, request.FILES)
        # check whether it's valid:
        if form.is_valid():
            form.save()
            return redirect('articles')
        # if a GET (or any other method) we'll create a blank form
    else:
        form = ArticlesForm()
    return render(request, 'insertarticles.html', {'form': form})

def selengkapnya(request, pk):
    satu_artikel = Articles.objects.get(id = pk)
    return render(request, 'selengkapnya.html', {'satu_artikel': satu_artikel})

def hapus_artikel(request, pk):
    satu_artikel = Articles.objects.get(id=pk)
    if request.method == "POST":
        satu_artikel.delete()
        return redirect("articles")
    context = {'satu_artikel': satu_artikel}
    return render(request, 'konfirmhapus.html', context)