from django.shortcuts import render, redirect
from kuesioner.forms import *

# Create your views here.
def formulir_kuesioner(request):
    form = KuesionerForm()
    if request.method == "POST":
        form = KuesionerForm(request.POST)
        if form.is_valid():
            hasil = Kuesioner.objects.create(**form.cleaned_data)
            return redirect('/hasilkuesioner/' + str(hasil.pk))
    context = {
        'form' : form
    }
    return render(request, 'kuesioner.html', context)

def list_kuesioner(request):
    querySet = Kuesioner.objects.all()
    context = {
        "kuesioner" : querySet
    }
    return render(request, 'list_kuesioner.html', context)

def hasil_kuesioner(request, pk):
    hasilKuesioner = Kuesioner.objects.get(id=pk)
    context = {
        "hasil_kuesioner": hasilKuesioner
    }
    return render(request, 'hasil_kuesioner.html', context)
