from django.db import models
from mptt.models import MPTTModel, TreeForeignKey

class Feedback(MPTTModel):
	user = models.CharField(max_length=35, default='anonymous')
	created = models.DateTimeField(auto_now_add=True)
	body = models.TextField()
	parent = TreeForeignKey('self', null=True, blank=True, on_delete=models.CASCADE, related_name='children')

	class MPTTMeta :
		order_insertion_by = ['created']

	def __str__(self):
		return f'Feedback by {self.user}'