from django.shortcuts import render, HttpResponseRedirect
from .models import Feedback
from .forms import FeedbackForm
# Create your views here.
def feedback(request):
	feedback = Feedback.objects.all()
	feedForm = FeedbackForm()
	context = {
		'feedbacks':feedback,
		'form':feedForm,
	}
	if request.method == "POST":
		feedForm = FeedbackForm(request.POST)
		if feedForm.is_valid():
			feedForm.save()
			return HttpResponseRedirect('/feedback')
	else:
		feedform = FeedbackForm()
	return render (request, 'feedback.html',context)
