from django.db import models

# Create your models here.

class Support(models.Model):
    email = models.EmailField(max_length=254)
    pesan = models.CharField(max_length=300)

    def __str__ (self):
        return self.email