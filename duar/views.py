from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse


def index(request):
    return render(request, 'landing_page.html')

def contact(request):
    return render(request, 'contact_page.html')