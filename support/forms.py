from django import forms
from .models import *
from crispy_forms.helper import FormHelper

class SupportForm(forms.ModelForm):
    email = forms.EmailField(label = 'Email Anda', max_length=254, widget=forms.TextInput(
                                                                                        attrs={'placeholder':'Your Email Please', 
                                                                                        'style': 'font-size: large; border-color: transparent; border-radius:30px' }))
    pesan = forms.CharField(label = 'Pesan Anda', max_length=500,  widget=forms.Textarea(
                                                                                        attrs={'placeholder':'Your Message Please',
                                                                                        'style': 'font-size: large; border-color: transparent; border-radius:30px'}))

    class Meta:
        model = Support
        fields = ('email', 'pesan')

    def __init__(self, *args, **kwargs):
        super(SupportForm, self).__init__(*args, **kwargs) # Call to ModelForm constructor
        self.helper = FormHelper()
        self.helper.form_id = 'id-supportForm'
        self.helper.form_class = 'form-control'