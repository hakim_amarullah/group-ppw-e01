# Group PPW E01

[![pipeline status](https://gitlab.com/hakim_amarullah/group-ppw-e01/badges/master/pipeline.svg)](https://gitlab.com/hakim_amarullah/group-ppw-e01/-/commits/master)

Repository ini berisi project kelompok PPW E01 yang mengembangkan website untuk self assesment Covid-19.
Alasan aplikasi ini dibuat ialah karena kebutuhan kantor akan suatu alat yang dapat membantu
pemetaan dari pandemi Covid-19 yang sedang terjadi. Aplikasi ini juga dapat membantu penggunanaya untuk
mengetahui risiko diri sendiri terpapar virus Corona. Website ini juga menyediakan form tanya dan jawab,
jika masih ada pertanyaan seputar Covid-19, jawabanya nantinya akan dikirim via email masing-masing pengguna.
Berikut link heroku app agar dapat diakses orang-orang di luar sana :D
[link to DuarApp!](https://duarpepew.herokuapp.com/)

DEVELOPER           | NPM
--------------------|-------------
Adam Syauqi         | 1906292881
Endriyani Rahayu    | 1906298866
Ariasena Cahya      | 1906292925
Hakim Amarullah     | 1906293051


