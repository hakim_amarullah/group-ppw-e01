from django.test import TestCase

# Create your tests here.
class MainPage(TestCase):
    def test_url_working(self):
        response = self.client.get('')
        self.assertEqual(response.status_code, 200)

    def test_using_template(self):
        response = self.client.get('')
        self.assertTemplateUsed(response, "landing_page.html")

    def test_url_contact_working(self):
        response = self.client.get('/contact/')
        self.assertEqual(response.status_code, 200)

    def test_contact_using_template(self):
        response = self.client.get('/contact/')
        self.assertTemplateUsed(response, "contact_page.html")