"""PPWGroupProject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from support import views as support
from feedback import views as feedback_views
from kuesioner import views as kuesioner
from articles import views as articles
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('duar.urls')),
    path('formulirsupport/', support.formulir_support, name = 'formulirsupport'),
    path('landingsupport/', support.landingsupport, name = 'landingsupport'),

    path('articles/', articles.articles_page, name = 'articles'),
    path('insertarticles/', articles.insert_articles, name = 'insertarticles'),
    path('articles/<str:pk>', articles.selengkapnya, name = 'selengkapnya'),
    path('deletearticle/<str:pk>', articles.hapus_artikel, name = 'hapus_artikel'),

    path('feedback/', feedback_views.feedback, name = 'feedback'),

    path('kuesioner/', kuesioner.formulir_kuesioner, name = 'kuesioner'),
    path('listkuesioner/', kuesioner.list_kuesioner, name = 'listkuesioner'),
    path('hasilkuesioner/<str:pk>/', kuesioner.hasil_kuesioner, name = 'hasilkuesioner'),
]

urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)


