from django.shortcuts import render, redirect
from .forms import SupportForm

# Create your views here.

def formulir_support(request):
    form = SupportForm()
    if request.method == "POST":
        form = SupportForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/formulirsupport')
    response = {'form' : form}
    return render(request, 'formulirsupport.html', response)

def landingsupport(request):
    response = {}
    return render(request, 'landingsupport.html', response)